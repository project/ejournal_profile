<?php

include_once('crud.inc.txt');

/************************************************************
*                           MODULES                         *
************************************************************/
function ejournal_profile_profile_modules() {
  return array (
    0 => 'admin_menu',
    1 => 'auto_nodetitle',
    2 => 'block',
    3 => 'captcha',
    4 => 'color',
    5 => 'comment',
    6 => 'content',
    7 => 'contact',
    8 => 'computed_field',
    9 => 'content_copy',
    10 => 'cvs_deploy',
    11 => 'date_api',
    12 => 'date',
  //  13 => 'devel',
    14 => 'drush',
    15 => 'update_status',
    16 => 'drush_pm',
    17 => 'drush_pm_svn',
    18 => 'drush_tools',
    19 => 'fckeditor',
    20 => 'fieldgroup',
    21 => 'mimedetect',
    22 => 'filter',
    23 => 'help',
    24 => 'imagecache',
    25 => 'imagefield',
    26 => 'login_destination',
    27 => 'menu',
    28 => 'filefield',
    29 => 'node',
    30 => 'nodereference',
    31 => 'number',
    32 => 'panels',
    33 => 'panels_export',
    34 => 'panels_mini',
    35 => 'panels_page',
    36 => 'views',
    37 => 'path',
    38 => 'token',
    39 => 'poormanscron',
    40 => 'search',
    41 => 'system',
    42 => 'taxonomy',
//    43 => 'taxonomy_context',
    44 => 'text',
    45 => 'text_captcha',
    46 => 'pathauto',
    47 => 'drush_pm_cvs',
    48 => 'user',
    49 => 'viewfield',
    50 => 'panels_views',
    51 => 'views_rss',
    52 => 'views_ui',
    53 => 'watchdog',
  );
}

/************************************************************
*                           DETAILS                         *
************************************************************/
function ejournal_profile_profile_details() {
  return array (
    'name' => 'E-Journal Install Profile',
    'description' => 'Allows the creation of a basic electronic journal',
  );
}

/************************************************************
*                           FINAL                           *
************************************************************/
function ejournal_profile_profile_final() {
  ejournal_profile_profile_final_variables();
  ejournal_profile_profile_final_roles();
  ejournal_profile_profile_final_users();
  ejournal_profile_profile_final_content_types();
  ejournal_profile_profile_final_taxonomy();
  ejournal_profile_profile_final_menus();
  ejournal_profile_profile_final_alias();
  ejournal_profile_profile_final_nodes();
  ejournal_profile_profile_final_blocks();
  ejournal_profile_profile_final_views();
  ejournal_profile_profile_final_cck();
  ejournal_profile_profile_final_panels();
  ejournal_profile_profile_final_fckeditor();
  ejournal_profile_profile_final_contact();
  ejournal_profile_profile_final_captcha();

  return;
}

function ejournal_profile_profile_final_nodes() {
  install_add_node((object)array(
    'type' => 'page',
    'status' => 1,
    'title' => 'About the Journal',
    'body' => 'Welcome to your new online journal. To log into the system click the login link and use the userid "admin" and the password "admin". <br /><br />Please edit this text to describe your journal.',
    'teaser' => 'Welcome to your new online journal. To log into the system click the login link and use the userid "admin" and the password "admin". <br /><br />Please edit this text to describe your journal.',
  ));
  install_add_node((object)array(
    'type' => 'page',
    'status' => 1,
    'format' => 2,
    'title' => 'RSS Feeds',
    'path' => 'rss_feeds',
    'body' => '
<?php
  print \'<h4 class="title">\' . \'What is RSS?\' . \'</h4>\';
?>
<p>RSS is method of subscribing to a particular information channel, such as a website, by means of software that aggregates news feeds (podcasts are in fact a variety of RSS feed).  One can use any modern web browser (Firefox, IE7, Safari) to track updates to a website without actually visiting the site itself, as new content is published, it notifies one\'s feed reader.  A number of <a href="http://en.wikipedia.org/wiki/Aggregator">alternative feed reader applications</a> are available.</p>
<p>This journal produces a number of RSS feeds. One may subscribe to all articles as they are published, or only receive notification when a new issue is released. Alternatively, one may follow a particular topic, regardless of issue or article organization.  See the comprehensive list below. Clicking on an icon or link will provide the URL needed to paste into an RSS feed reader.</p>
<?php
   global $base_url;
   $feed_icon = \' <img src="\' . $base_url . \'/misc/feed.png" alt="RSS Icon"/>\';

  $feeds = array();
  $feeds[] = l(\'Issues\'.$feed_icon, \'issues/feed\', array(), NULL, FALSE, FALSE, TRUE);
  $feeds[] = l(\'Articles\'.$feed_icon, \'articles/feed\', array(), NULL, FALSE, FALSE, TRUE);
  $feeds[] = l(\'Editor\\\'s Blog\'.$feed_icon, \'editors_blog/feed\', array(), NULL, FALSE, FALSE, TRUE);
  print theme(\'item_list\', $feeds, \'General Feeds\', \'ul\', array(\'style\' => \'margin-bottom: 10px;\'));
  
  $topic_feeds = array();
  $topic_result = db_query("SELECT * FROM {term_data} WHERE vid = %d", 2);
  while ($topic = db_fetch_object($topic_result)) {
    $topic_feeds[] = l($topic->name.$feed_icon, \'taxonomy/term/\'. $topic->tid.\'/feed\', array(), NULL, FALSE, FALSE, TRUE);
  }
  print theme(\'item_list\', $topic_feeds, \'Topic Feeds\', \'ul\',array(\'style\' => \'margin-bottom: 10px;\'));
?>
    ',
    'teaser' => '
<?php
  print \'<h4 class="title">\' . \'What is RSS?\' . \'</h4>\';
?>
  <p>RSS is method of subscribing to a particular information channel, such as a website, by means of software that aggregates news feeds (podcasts are in fact a variety of RSS feed).  One can use any modern web browser (Firefox, IE7, Safari) to track updates to a website without actually visiting the site itself, as new content is published, it notifies one\'s feed reader.  A number of <a href="http://en.wikipedia.org/wiki/Aggregator">alternative feed reader applications</a> are available.</p>
  <p>This journal produces a number of RSS feeds. One may subscribe to all articles as they are published, or only receive notification when a new issue is released. Alternatively, one may follow a particular topic, regardless of issue or article organization.  See the comprehensive list below. Clicking on an icon or link will provide the URL needed to paste into an RSS feed reader.</p>
<?php
   global $base_url;
   $feed_icon = \' <img src="\' . $base_url . \'/misc/feed.png" alt="RSS Icon"/>\';

  $feeds = array();
  $feeds[] = l(\'Issues\'.$feed_icon, \'issues/feed\', array(), NULL, FALSE, FALSE, TRUE);
  $feeds[] = l(\'Articles\'.$feed_icon, \'articles/feed\', array(), NULL, FALSE, FALSE, TRUE);
  $feeds[] = l(\'Editor\\\'s Blog\'.$feed_icon, \'editors_blog/feed\', array(), NULL, FALSE, FALSE, TRUE);
  print theme(\'item_list\', $feeds, \'General Feeds\', \'ul\', array(\'style\' => \'margin-bottom: 10px;\'));
  
  $topic_feeds = array();
  $topic_result = db_query("SELECT * FROM {term_data} WHERE vid = %d", 2);
  while ($topic = db_fetch_object($topic_result)) {
    $topic_feeds[] = l($topic->name.$feed_icon, \'taxonomy/term/\'. $topic->tid.\'/feed\', array(), NULL, FALSE, FALSE, TRUE);
  }
  print theme(\'item_list\', $topic_feeds, \'Topic Feeds\', \'ul\',array(\'style\' => \'margin-bottom: 10px;\'));
?>
    ',
  ));
}

function ejournal_profile_profile_final_taxonomy() {
  install_add_vocabulary(2, 'Topics', '', '', 0, 0, 0, 0, 0, 'taxonomy', 0);
  install_add_vocabulary_node_type(2, 'article');
}

function ejournal_profile_profile_final_content_types() {
  install_add_content_type(array (
    'type' => 'article',
    'name' => 'Article',
    'module' => 'node',
    'description' => 'The principal content type of the site - a refereed online academic journal article. ',
    'help' => '',
    'has_title' => '1',
    'title_label' => 'Title of Article',
    'has_body' => '1',
    'body_label' => 'Body of Article',
    'min_word_count' => '0',
    'custom' => '1',
    'modified' => '1',
    'locked' => '0',
    'orig_type' => '',
  ));
  install_add_content_type(array (
    'type' => 'author',
    'name' => 'Author',
    'module' => 'node',
    'description' => 'A profile of an author of an article on the system',
    'help' => '',
    'has_title' => '1',
    'title_label' => 'Name of Author',
    'has_body' => '1',
    'body_label' => 'Biography of Author',
    'min_word_count' => '0',
    'custom' => '1',
    'modified' => '1',
    'locked' => '0',
    'orig_type' => '',
  ));
  install_add_content_type(array (
    'type' => 'issue',
    'name' => 'Issue',
    'module' => 'node',
    'description' => 'An issue of the refereed online academic journal organized by volume',
    'help' => '',
    'has_title' => '1',
    'title_label' => 'Title of Issue',
    'has_body' => '0',
    'body_label' => '',
    'min_word_count' => '0',
    'custom' => '1',
    'modified' => '1',
    'locked' => '0',
    'orig_type' => '',
  ));
  install_add_content_type(array (
    'type' => 'page',
    'name' => 'Page',
    'module' => 'node',
    'description' => 'If you want to add a static page, like a contact page or an about page, use a page.',
    'help' => '',
    'has_title' => '1',
    'title_label' => 'Title',
    'has_body' => '1',
    'body_label' => 'Body',
    'min_word_count' => '0',
    'custom' => '1',
    'modified' => '1',
    'locked' => '0',
    'orig_type' => 'page',
  ));
  install_add_content_type(array (
    'type' => 'story',
    'name' => 'Story',
    'module' => 'node',
    'description' => 'Stories are articles in their simplest form: they have a title, a teaser and a body, but can be extended by other modules. The teaser is part of the body too. Stories may be used as a personal blog or for news articles.',
    'help' => '',
    'has_title' => '1',
    'title_label' => 'Title',
    'has_body' => '1',
    'body_label' => 'Body',
    'min_word_count' => '0',
    'custom' => '1',
    'modified' => '1',
    'locked' => '0',
    'orig_type' => 'story',
  ));
}

function ejournal_profile_profile_final_roles() {
  $roles = array();
  
  $roles['administrative user'] = install_add_role('administrative user');
  $roles['editorial user'] = install_add_role('editorial user');
  
  install_set_permissions(DRUPAL_ANONYMOUS_RID, array (
    0 => 'access comments',
    1 => ' access site-wide contact form',
    2 => ' view filefield uploads',
    3 => ' access content',
    4 => ' search content',
    5 => ' use advanced search',
  ));
  install_set_permissions(DRUPAL_AUTHENTICATED_RID, array (
    0 => 'access comments',
    1 => ' post comments',
    2 => ' post comments without approval',
    3 => ' access site-wide contact form',
    4 => ' access fckeditor',
    5 => ' view filefield uploads',
    6 => ' access content',
    7 => ' search content',
    8 => ' use advanced search',
  ));
  install_set_permissions($roles['administrative user'], array (
    0 => 'access administration menu',
    1 => ' use PHP for title patterns',
    2 => ' administer blocks',
    3 => ' use PHP for block visibility',
    4 => ' administer CAPTCHA settings',
    5 => ' access comments',
    6 => ' administer comments',
    7 => ' post comments',
    8 => ' post comments without approval',
    9 => ' access site-wide contact form',
    10 => ' access fckeditor',
    11 => ' administer fckeditor',
    12 => ' view filefield uploads',
    13 => ' administer filters',
    14 => ' administer imagecache',
    15 => ' flush imagecache',
    16 => ' administer menu',
    17 => ' access content',
    18 => ' administer content types',
    19 => ' administer nodes',
    20 => ' create article content',
    21 => ' create author content',
    22 => ' create datasheet content',
    23 => ' create issue content',
    24 => ' create page content',
    25 => ' create story content',
    26 => ' edit article content',
    27 => ' edit author content',
    28 => ' edit datasheet content',
    29 => ' edit issue content',
    30 => ' edit own article content',
    31 => ' edit own author content',
    32 => ' edit own datasheet content',
    33 => ' edit own issue content',
    34 => ' edit own page content',
    35 => ' edit own story content',
    36 => ' edit page content',
    37 => ' edit story content',
    38 => ' revert revisions',
    39 => ' view revisions',
    40 => ' administer advanced pane settings',
    41 => ' administer pane access',
    42 => ' administer pane visibility',
    43 => ' view all panes',
    44 => ' administer mini panels',
    45 => ' create mini panels',
    46 => ' access all panel-pages',
    47 => ' create panel-pages',
    48 => ' administer panel views',
    49 => ' administer url aliases',
    50 => ' create url aliases',
    51 => ' administer pathauto',
    52 => ' administer search',
    53 => ' search content',
    54 => ' use advanced search',
    55 => ' access administration pages',
    56 => ' administer site configuration',
    57 => ' select different theme',
    58 => ' administer taxonomy',
    59 => ' administer access control',
    60 => ' administer users',
    61 => ' access all views',
    62 => ' administer views',
  ));
  install_set_permissions($roles['editorial user'], array (
    0 => 'access comments',
    1 => ' administer comments',
    2 => ' post comments',
    3 => ' post comments without approval',
    4 => ' access site-wide contact form',
    5 => ' access fckeditor',
    6 => ' view filefield uploads',
    7 => ' access content',
    8 => ' administer nodes',
    9 => ' create article content',
    10 => ' create author content',
    11 => ' create datasheet content',
    12 => ' create issue content',
    13 => ' create page content',
    14 => ' create story content',
    15 => ' edit article content',
    16 => ' edit author content',
    17 => ' edit datasheet content',
    18 => ' edit issue content',
    19 => ' edit own article content',
    20 => ' edit own author content',
    21 => ' edit own datasheet content',
    22 => ' edit own issue content',
    23 => ' edit own page content',
    24 => ' edit own story content',
    25 => ' edit page content',
    26 => ' edit story content',
    27 => ' revert revisions',
    28 => ' view revisions',
    29 => ' administer advanced pane settings',
    30 => ' administer pane access',
    31 => ' administer pane visibility',
    32 => ' view all panes',
    33 => ' administer mini panels',
    34 => ' create mini panels',
    35 => ' access all panel-pages',
    36 => ' create panel-pages',
    37 => ' administer panel views',
    38 => ' administer url aliases',
    39 => ' create url aliases',
    40 => ' administer pathauto',
    41 => ' search content',
    42 => ' use advanced search',
    43 => ' administer taxonomy',
    44 => ' access all views',
    45 => ' administer views',
  ));
}

function ejournal_profile_profile_final_users() {
  install_add_user('admin', 'admin', 'admin@ejournalsite.com', array (
    0 => 'administrative user',
    1 => 'editorial user',
  ), 1);
}

function ejournal_profile_profile_final_menus() {
  // Primary links
  install_menu_create_menu_items(array (
    0 => 
    array (
      'path' => 'user/login',
      'title' => 'Login',
      'description' => 'Login to the Site',
      'weight' => '10',
      'type' => '118',
      'children' => 
      array (
      ),
    ),
    1 => 
    array (
      'path' => 'rss_feeds',
      'title' => 'RSS',
      'description' => 'The RSS Feeds of the Site',
      'weight' => '0',
      'type' => '118',
      'children' => 
      array (
      ),
    ),
    2 => 
    array (
      'path' => 'logout',
      'title' => 'Logout',
      'description' => 'Logout',
      'weight' => '10',
      'type' => '118',
      'children' => 
      array (
      ),
    ),
    3 => 
    array (
      'path' => 'issues',
      'title' => 'Issues',
      'description' => 'Issues',
      'weight' => '-8',
      'type' => '118',
      'children' => 
      array (
      ),
    ),
    4 => 
    array (
      'path' => '<front>',
      'title' => 'Home',
      'description' => 'Home',
      'weight' => '-10',
      'type' => '118',
      'children' => 
      array (
      ),
    ),
    5 => 
    array (
      'path' => 'editors_blog',
      'title' => 'Editor\'s Blog',
      'description' => 'Editor\'s Blog',
      'weight' => '-8',
      'type' => '118',
      'children' => 
      array (
      ),
    ),
    6 => 
    array (
      'path' => 'admin/journal',
      'title' => 'Admin',
      'description' => 'Admin Journal',
      'weight' => '8',
      'type' => '118',
      'children' => 
      array (
        0 => 
        array (
          'path' => 'admin/journal/issues',
          'title' => 'Manage Issues',
          'description' => 'Manage Issues',
          'weight' => '-5',
          'type' => '374',
          'children' => 
          array (
          ),
        ),
        1 => 
        array (
          'path' => 'admin/journal/articles',
          'title' => 'Manage Articles',
          'description' => 'Manage Articles',
          'weight' => '1',
          'type' => '118',
          'children' => 
          array (
          ),
        ),
        2 => 
        array (
          'path' => 'admin/journal/authors',
          'title' => 'Manage Authors',
          'description' => 'Manage Authors',
          'weight' => '5',
          'type' => '118',
          'children' => 
          array (
          ),
        ),
        3 => 
        array (
          'path' => 'admin/journal/blog',
          'title' => 'Manage Blog',
          'description' => 'Manage Blog',
          'weight' => '8',
          'type' => '118',
          'children' => 
          array (
          ),
        ),
        4 => 
        array (
          'path' => 'admin/content/taxonomy/2',
          'title' => 'Manage Topics',
          'description' => 'Manage Topics',
          'weight' => '9',
          'type' => '118',
          'children' => 
          array (
          ),
        ),
        5 => 
        array (
          'path' => 'admin/journal/datasheets',
          'title' => 'Manage Datasheets',
          'description' => 'Manage Datasheets',
          'weight' => '10',
          'type' => '118',
          'children' => 
          array (
          ),
        ),
      ),
    ),
    7 => 
    array (
      'path' => 'contact',
      'title' => 'Contact',
      'description' => 'Contact',
      'weight' => '6',
      'type' => '118',
      'children' => 
      array (
      ),
    ),
  ),2);

  // Other menus
  install_menu_create_menu_items(array (
    0 => 
    array (
      'path' => '',
      'title' => 'Getting Started',
      'description' => '',
      'weight' => '0',
      'type' => '115',
      'children' => 
      array (
        0 => 
        array (
          'path' => '#',
          'title' => 'Documentation',
          'description' => '',
          'weight' => '0',
          'type' => '118',
          'children' => 
          array (
          ),
        ),
        1 => 
        array (
          'path' => 'node/add/article',
          'title' => 'New Article',
          'description' => '',
          'weight' => '2',
          'type' => '118',
          'children' => 
          array (
          ),
        ),
        2 => 
        array (
          'path' => 'node/add/issue',
          'title' => 'New Issue',
          'description' => '',
          'weight' => '4',
          'type' => '118',
          'children' => 
          array (
          ),
        ),
        3 => 
        array (
          'path' => 'node/add/author',
          'title' => 'New Author',
          'description' => '',
          'weight' => '6',
          'type' => '118',
          'children' => 
          array (
          ),
        ),
      ),
    ),
  ),0);
}

function ejournal_profile_profile_final_alias() {
  db_query(
      "INSERT INTO {url_alias} (src,dst)
      VALUES ('%s','%s')",
      'node/1','about'
  );
}

function ejournal_profile_profile_final_fckeditor() {
  db_query("DELETE FROM {fckeditor_role}");
  db_query("INSERT INTO {fckeditor_role} (name, rid) VALUES ('Advanced',2)");
  db_query("INSERT INTO {fckeditor_role} (name, rid) VALUES ('Advanced',3)");
  db_query("INSERT INTO {fckeditor_role} (name, rid) VALUES ('Advanced',4)");
    
  $settings['Advanced'] = array (
    'old_name' => 'Advanced',
    'name' => 'Advanced',
    'rids' => 
    array (
      3 => '3',
      2 => '2',
      4 => '4',
    ),
    'allow_user_conf' => 'f',
    'min_rows' => '1',
    'excl_mode' => '0',
    'excl_fields' => '',
    'excl_paths' => 'admin/content/types/article/fields/field_article_citation',
    'simple_incl_fields' => '',
    'simple_incl_paths' => '',
    'default' => 't',
    'show_toggle' => 't',
    'popup' => 'f',
    'skin' => 'default',
    'toolbar' => 'DrupalFiltered',
    'expand' => 't',
    'width' => '100%',
    'lang' => 'en',
    'auto_lang' => 't',
    'enter_mode' => 'p',
    'shift_enter_mode' => 'br',
    'font_format' => 'p;div;pre;address;h1;h2;h3;h4;h5;h6',
    'format_source' => 't',
    'format_output' => 't',
    'css_mode' => 'theme',
    'css_path' => '',
    'css_style' => 'theme',
    'styles_path' => '',
    'upload_basic' => 'f',
    'upload_advanced' => 'f',
    'UserFilesPath' => '%b%f/',
    'UserFilesAbsolutePath' => '%d%b%f/',
    'js_conf' => '',
    'op' => 'Update profile',
    'form_token' => 'b040e7ad50a30f6de84c7a96a597021e',
    'form_id' => 'fckeditor_profile_form_build',
  );
  $settings['FCKeditor Global Profile'] = array (
    'old_name' => 'FCKeditor Global Profile',
    'rank' => 
    array (
      0 => '4',
      1 => '3',
      2 => '2',
    ),
    'excl_mode' => '0',
    'excl_fields' => 'edit-user-mail-welcome-body
edit-user-mail-admin-body
edit-user-mail-approval-body
edit-user-mail-pass-body
edit-user-mail-register-admin-created-body
edit-user-mail-register-no-approval-required-body
edit-user-mail-register-pending-approval-body
edit-user-mail-password-reset-body
edit-user-mail-status-activated-body
edit-user-mail-status-blocked-body
edit-user-mail-status-deleted-body
edit-pages
edit-recipients
edit-reply
edit-description
edit-synonyms
edit-img-assist-textareas
edit-nodewords-description
edit-allowed-values-php
edit-allowed-values
edit-code',
  'excl_paths' => '',
  'simple_incl_fields' => 'edit-signature
edit-site-mission
edit-site-footer
edit-site-offline-message
edit-page-help
edit-user-registration-help
edit-user-picture-guidelines
',
    'simple_incl_paths' => '',
    'op' => 'Update global profile',
    'form_token' => '74a1917c58d54bfab425ff6481268e9a',
    'form_id' => 'fckeditor_global_profile_form_build',
    'name' => 'FCKeditor Global Profile',
  );
    
  db_query("DELETE FROM {fckeditor_settings}");
  foreach ($settings as $name => $setting) {
    db_query("INSERT INTO {fckeditor_settings} (name, settings) VALUES ('%s','%s')", $name, serialize($setting));
  }
}

function ejournal_profile_profile_final_contact() {
  db_query("INSERT INTO {contact} (cid, category, recipients, reply, weight, selected) VALUES (1,'Sitewide Contact Form','matt@chapterthree.com','',0,1)");
}

function ejournal_profile_profile_final_captcha() {
  db_query("DELETE FROM {captcha_points}");
  db_query("INSERT INTO {captcha_points} (form_id, module, type) VALUES ('comment_form',NULL,NULL)");
  db_query("INSERT INTO {captcha_points} (form_id, module, type) VALUES ('contact_mail_user',NULL,NULL)");
  db_query("INSERT INTO {captcha_points} (form_id, module, type) VALUES ('contact_mail_page',NULL,NULL)");
  db_query("INSERT INTO {captcha_points} (form_id, module, type) VALUES ('user_register','text_captcha','Text')");
  db_query("INSERT INTO {captcha_points} (form_id, module, type) VALUES ('user_pass',NULL,NULL)");
  db_query("INSERT INTO {captcha_points} (form_id, module, type) VALUES ('user_login',NULL,NULL)");
  db_query("INSERT INTO {captcha_points} (form_id, module, type) VALUES ('user_login_block',NULL,NULL)");
}

function ejournal_profile_profile_final_views() {
  include_once('ejournal_profile.views.inc.txt');
  if (function_exists('ejournal_profile_profile_views') && function_exists('ejournal_profile_profile_import_view')) {
    $views = ejournal_profile_profile_views();
    foreach ($views as $view) {
      ejournal_profile_profile_import_view($view);
    }
  }
}

function ejournal_profile_profile_final_cck() {
  include_once('ejournal_profile.cck.inc.txt');
  if (function_exists('ejournal_profile_profile_cck_fields') && function_exists('ejournal_profile_profile_import_cck_fields')) {
    foreach (ejournal_profile_profile_cck_fields() as $type => $content) {
      ejournal_profile_profile_import_cck_fields($type, $content);
    }
  }
  content_clear_type_cache();
}

function ejournal_profile_profile_final_panels() {
  include_once('ejournal_profile.panels.inc.txt');
  if (function_exists('ejournal_profile_profile_default_panel_minis') && function_exists('panels_mini_save')) {
    foreach (ejournal_profile_profile_default_panel_minis() as $panel) {
      panels_mini_save($panel);
    }
  }
  if (function_exists('ejournal_profile_profile_default_panel_pages') && function_exists('panels_page_save')) {
    foreach (ejournal_profile_profile_default_panel_pages() as $panel) {
      panels_page_save($panel);
    }
  }
  if (function_exists('ejournal_profile_profile_default_panel_views') && function_exists('panels_views_save')) {
    foreach (ejournal_profile_profile_default_panel_views() as $panel) {
      //drupal_set_message(print_r($panel, TRUE));
      panels_views_save($panel);
    }
  }
}

function ejournal_profile_profile_final_variables() {
  variable_set('anonymous', 'Anonymous');
  variable_set('ant_article', '0');
  variable_set('ant_author', '1');
  variable_set('ant_datasheet', '1');
  variable_set('ant_issue', '1');
  variable_set('ant_pattern_article', '');
  variable_set('ant_pattern_author', '[field_author_firstname-raw] [field_author_lastname-raw]');
  variable_set('ant_pattern_datasheet', '[field_datasheet_attachment-filename]');
  variable_set('ant_pattern_issue', 'Volume [field_issue_volume_number-raw], Issue [field_issue_issue_number-raw]');
  variable_set('ant_php_article', 0);
  variable_set('ant_php_author', 0);
  variable_set('ant_php_datasheet', 0);
  variable_set('ant_php_issue', 0);
  variable_set('captcha_administration_mode', 0);
  variable_set('captcha_description', 'This question is for testing whether you are a human visitor and to prevent automated spam submissions.');
  variable_set('captcha_log_wrong_responses', 0);
  variable_set('captcha_persistence', '1');
  variable_set('captcha_wrong_response_counter', 1);
  variable_set('clean_url', '1');
  variable_set('comment_anonymous', 0);
  variable_set('comment_article', '2');
  variable_set('comment_author', '0');
  variable_set('comment_controls', '3');
  variable_set('comment_datasheet', '0');
  variable_set('comment_default_mode', '4');
  variable_set('comment_default_order', '1');
  variable_set('comment_default_per_page', '50');
  variable_set('comment_form_location', '0');
  variable_set('comment_issue', '0');
  variable_set('comment_page', 0);
  variable_set('comment_preview', '0');
  variable_set('comment_subject_field', '1');
  variable_set('content_update_1009', true);
  variable_set('date_default_timezone_name', 'Africa/Addis_Ababa');
  variable_set('default_nodes_main', '10');
  variable_set('file_directory_temp', '/tmp');
  variable_set('filter_default_format', '3');
  variable_set('filter_html_1', 1);
  variable_set('htmlbox_buttons_order', 'bold, italic, separator_dots, hyperlink, separator_dots, ul, ol, separator_dots, indent, outdent, separator_dots, html');
  variable_set('htmlbox_custom_fields', '');
  variable_set('htmlbox_newline_removal', 1);
  variable_set('htmlbox_node_forms', array (
    'article' => 'article',
    'author' => 'author',
    'issue' => 'issue',
    'page' => 'page',
    'story' => 'story',
  ));
  variable_set('htmlbox_node_forms_body_only', 0);
  variable_set('htmlbox_other_forms', array (
    'comment_form' => 'comment_form',
    'contact_mail_page' => 0,
    'contact_mail_user' => 0,
    'taxonomy_form_vocabulary' => 0,
    'taxonomy_form_term' => 0,
    'any' => 0,
  ));
  variable_set('htmlbox_xhtml_parser', 0);
  variable_set('ld_condition_snippet', '');
  variable_set('ld_condition_type', 'always');
  variable_set('ld_url_destination', '<front>');
  variable_set('ld_url_type', 'static');
  variable_set('menu_primary_menu', 2);
  variable_set('menu_secondary_menu', 2);
  variable_set('minimum_word_size', '3');
  variable_set('node_cron_comments_scale', 0.5);
  variable_set('node_cron_last', '1225164651');
  variable_set('node_cron_last_nid', '52');
  variable_set('node_cron_views_scale', 1);
  variable_set('node_options_article', array (
    0 => 'status',
    1 => 'promote',
  ));
  variable_set('node_options_author', array (
    0 => 'status',
    1 => 'promote',
  ));
  variable_set('node_options_datasheet', array (
    0 => 'status',
    1 => 'promote',
  ));
  variable_set('node_options_topic', array (
    0 => 'status',
  ));
  variable_set('node_options_issue', array (
    0 => 'status',
    1 => 'promote',
  ));
  variable_set('node_options_page', array (
    0 => 'status',
  ));
  variable_set('node_preview', '0');
  variable_set('node_rank_comments', '0');
  variable_set('node_rank_recent', '0');
  variable_set('node_rank_relevance', '5');
  variable_set('overlap_cjk', 1);
  variable_set('pathauto_blog_applytofeeds', 'feed');
  variable_set('pathauto_blog_bulkupdate', 0);
  variable_set('pathauto_blog_pattern', 'blogs/[user-raw]');
  variable_set('pathauto_blog_supportsfeeds', 'feed');
  variable_set('pathauto_case', '1');
  variable_set('pathauto_ignore_words', 'a,an,as,at,before,but,by,for,from,is,in,into,like,of,off,on,onto,per,since,than,the,this,that,to,up,via,with');
  variable_set('pathauto_indexaliases', false);
  variable_set('pathauto_indexaliases_bulkupdate', false);
  variable_set('pathauto_max_bulk_update', '50');
  variable_set('pathauto_max_component_length', '100');
  variable_set('pathauto_max_length', '100');
  variable_set('pathauto_modulelist', array (
    0 => 'node',
    1 => 'taxonomy',
    2 => 'user',
  ));
  variable_set('pathauto_node_applytofeeds', '');
  variable_set('pathauto_node_article_pattern', 'articles/[title-raw]-by-[field_article_author-title]');
  variable_set('pathauto_node_author_pattern', 'authors/[title-raw]');
  variable_set('pathauto_node_blog_pattern', '');
  variable_set('pathauto_node_bulkupdate', 0);
  variable_set('pathauto_node_topic_pattern', '');
  variable_set('pathauto_node_image_pattern', '');
  variable_set('pathauto_node_issue_pattern', 'issues/volume-[field_issue_volume_number-raw]/issue-[field_issue_issue_number-raw]');
  variable_set('pathauto_node_page_pattern', '');
  variable_set('pathauto_node_pattern', '');
  variable_set('pathauto_node_story_pattern', 'blog/[title-raw]');
  variable_set('pathauto_node_supportsfeeds', 'feed');
  variable_set('pathauto_punctuation_ampersand', '0');
  variable_set('pathauto_punctuation_asterisk', '0');
  variable_set('pathauto_punctuation_at', '0');
  variable_set('pathauto_punctuation_backtick', '0');
  variable_set('pathauto_punctuation_back_slash', '0');
  variable_set('pathauto_punctuation_caret', '0');
  variable_set('pathauto_punctuation_colon', '0');
  variable_set('pathauto_punctuation_comma', '0');
  variable_set('pathauto_punctuation_dollar', '0');
  variable_set('pathauto_punctuation_double_quotes', '0');
  variable_set('pathauto_punctuation_equal', '0');
  variable_set('pathauto_punctuation_exclamation', '0');
  variable_set('pathauto_punctuation_greater_than', '0');
  variable_set('pathauto_punctuation_hash', '0');
  variable_set('pathauto_punctuation_hyphen', '1');
  variable_set('pathauto_punctuation_left_curly', '0');
  variable_set('pathauto_punctuation_left_parenthesis', '0');
  variable_set('pathauto_punctuation_left_square', '0');
  variable_set('pathauto_punctuation_less_than', '0');
  variable_set('pathauto_punctuation_percent', '0');
  variable_set('pathauto_punctuation_period', '0');
  variable_set('pathauto_punctuation_pipe', '0');
  variable_set('pathauto_punctuation_plus', '0');
  variable_set('pathauto_punctuation_question_mark', '0');
  variable_set('pathauto_punctuation_quotes', '0');
  variable_set('pathauto_punctuation_right_curly', '0');
  variable_set('pathauto_punctuation_right_parenthesis', '0');
  variable_set('pathauto_punctuation_right_square', '0');
  variable_set('pathauto_punctuation_semicolon', '0');
  variable_set('pathauto_punctuation_tilde', '0');
  variable_set('pathauto_punctuation_underscore', '0');
  variable_set('pathauto_reduce_ascii', 0);
  variable_set('pathauto_separator', '-');
  variable_set('pathauto_taxonomy_1_pattern', '');
  variable_set('pathauto_taxonomy_2_pattern', '');
  variable_set('pathauto_taxonomy_applytofeeds', '');
  variable_set('pathauto_taxonomy_bulkupdate', 0);
  variable_set('pathauto_taxonomy_pattern', 'category/[vocab-raw]/[catpath-raw]');
  variable_set('pathauto_taxonomy_supportsfeeds', '0/feed');
  variable_set('pathauto_transliterate', false);
  variable_set('pathauto_update_action', '2');
  variable_set('pathauto_user_bulkupdate', 0);
  variable_set('pathauto_user_pattern', 'users/[user-raw]');
  variable_set('pathauto_user_supportsfeeds', NULL);
  variable_set('pathauto_verbose', 0);
  variable_set('poormanscron_lastrun', 1225221281);
  variable_set('search_cron_limit', '100');
  variable_set('site_footer', '');
  variable_set('site_frontpage', 'homepage');
  variable_set('site_mail', 'matt@chapterthree.com');
  variable_set('site_mission', '');
  variable_set('site_name', 'Journal Title');
  variable_set('site_slogan', '');
  variable_set('taxonomy_context_block_display', '1');
  variable_set('taxonomy_context_block_display_1', '1');
  variable_set('taxonomy_context_block_display_2', '1');
  variable_set('taxonomy_context_breadcrumb_article', '0');
  variable_set('taxonomy_context_breadcrumb_author', '0');
  variable_set('taxonomy_context_breadcrumb_datasheet', '0');
  variable_set('taxonomy_context_breadcrumb_issue', '0');
  variable_set('taxonomy_context_inline_article', '0');
  variable_set('taxonomy_context_inline_author', '0');
  variable_set('taxonomy_context_inline_datasheet', '0');
  variable_set('taxonomy_context_inline_issue', '0');
  variable_set('taxonomy_context_node_block', '0');
  variable_set('taxonomy_context_show_subterms', '1');
  variable_set('taxonomy_context_show_term', '1');
  variable_set('taxonomy_context_use_style', '1');
  variable_set('teaser_length', '400');
  system_theme_data();
  db_query("UPDATE {system} SET status = 1 WHERE type = 'theme' and name = '%s'", 'garland');
  variable_set('theme_default', 'garland');
  variable_set('theme_garland_settings', array (
    'toggle_logo' => 1,
    'toggle_name' => 1,
    'toggle_slogan' => 0,
    'toggle_mission' => 0,
    'toggle_node_user_picture' => 0,
    'toggle_comment_user_picture' => 0,
    'toggle_search' => 1,
    'toggle_favicon' => 0,
    'default_logo' => 1,
    'logo_path' => '',
    'logo_upload' => '',
    'default_favicon' => 0,
    'favicon_path' => '',
    'favicon_upload' => '',
    'op' => 'Save configuration',
    'form_token' => '2f7d5f7967061e0034f2b23e98469d5b',
    'scheme' => '',
    'palette' => 
    array (
      'base' => '#efefd2',
      'link' => '#b9092a',
      'top' => '#e71e08',
      'bottom' => '#45030e',
      'text' => '#3b3b3c',
    ),
    'theme' => 'garland',
    'info' => 
    array (
      'schemes' => 
      array (
        '#0072b9,#027ac6,#2385c2,#5ab5ee,#494949' => 'Blue Lagoon (Default)',
        '#464849,#2f416f,#2a2b2d,#5d6779,#494949' => 'Ash',
        '#55c0e2,#000000,#085360,#007e94,#696969' => 'Aquamarine',
        '#d5b048,#6c420e,#331900,#971702,#494949' => 'Belgian Chocolate',
        '#3f3f3f,#336699,#6598cb,#6598cb,#000000' => 'Bluemarine',
        '#d0cb9a,#917803,#efde01,#e6fb2d,#494949' => 'Citrus Blast',
        '#0f005c,#434f8c,#4d91ff,#1a1575,#000000' => 'Cold Day',
        '#c9c497,#0c7a00,#03961e,#7be000,#494949' => 'Greenbeam',
        '#ffe23d,#a9290a,#fc6d1d,#a30f42,#494949' => 'Mediterrano',
        '#788597,#3f728d,#a9adbc,#d4d4d4,#707070' => 'Mercury',
        '#5b5fa9,#5b5faa,#0a2352,#9fa8d5,#494949' => 'Nocturnal',
        '#7db323,#6a9915,#b5d52a,#7db323,#191a19' => 'Olivia',
        '#12020b,#1b1a13,#f391c6,#f41063,#898080' => 'Pink Plastic',
        '#b7a0ba,#c70000,#a1443a,#f21107,#515d52' => 'Shiny Tomato',
        '#18583d,#1b5f42,#34775a,#52bf90,#2d2d2d' => 'Teal Top',
        '' => 'Custom',
      ),
      'copy' => 
      array (
        0 => 'images/menu-collapsed.gif',
        1 => 'images/menu-expanded.gif',
        2 => 'images/menu-leaf.gif',
      ),
      'gradient' => 
      array (
        0 => 0,
        1 => 37,
        2 => 760,
        3 => 121,
      ),
      'fill' => 
      array (
        'base' => 
        array (
          0 => 0,
          1 => 0,
          2 => 760,
          3 => 568,
        ),
        'link' => 
        array (
          0 => 107,
          1 => 533,
          2 => 41,
          3 => 23,
        ),
      ),
      'slices' => 
      array (
        'images/body.png' => 
        array (
          0 => 0,
          1 => 37,
          2 => 1,
          3 => 280,
        ),
        'images/bg-bar.png' => 
        array (
          0 => 202,
          1 => 530,
          2 => 76,
          3 => 14,
        ),
        'images/bg-bar-white.png' => 
        array (
          0 => 202,
          1 => 506,
          2 => 76,
          3 => 14,
        ),
        'images/bg-tab.png' => 
        array (
          0 => 107,
          1 => 533,
          2 => 41,
          3 => 23,
        ),
        'images/bg-navigation.png' => 
        array (
          0 => 0,
          1 => 0,
          2 => 7,
          3 => 37,
        ),
        'images/bg-content-left.png' => 
        array (
          0 => 40,
          1 => 117,
          2 => 50,
          3 => 352,
        ),
        'images/bg-content-right.png' => 
        array (
          0 => 510,
          1 => 117,
          2 => 50,
          3 => 352,
        ),
        'images/bg-content.png' => 
        array (
          0 => 299,
          1 => 117,
          2 => 7,
          3 => 200,
        ),
        'images/bg-navigation-item.png' => 
        array (
          0 => 32,
          1 => 37,
          2 => 17,
          3 => 12,
        ),
        'images/bg-navigation-item-hover.png' => 
        array (
          0 => 54,
          1 => 37,
          2 => 17,
          3 => 12,
        ),
        'images/gradient-inner.png' => 
        array (
          0 => 646,
          1 => 307,
          2 => 112,
          3 => 42,
        ),
        'logo.png' => 
        array (
          0 => 622,
          1 => 51,
          2 => 64,
          3 => 73,
        ),
        'screenshot.png' => 
        array (
          0 => 0,
          1 => 37,
          2 => 400,
          3 => 240,
        ),
      ),
      'blend_target' => '#ffffff',
      'preview_image' => 'color/preview.png',
      'preview_css' => 'color/preview.css',
      'base_image' => 'color/base.png',
    ),
  ));
  variable_set('theme_settings', array (
    'toggle_logo' => 1,
    'toggle_name' => 1,
    'toggle_slogan' => 0,
    'toggle_mission' => 1,
    'toggle_node_user_picture' => 0,
    'toggle_comment_user_picture' => 0,
    'toggle_search' => 1,
    'toggle_favicon' => 1,
    'toggle_node_info_article' => 0,
    'toggle_node_info_author' => 0,
    'toggle_node_info_blog' => 1,
    'toggle_node_info_issue' => 0,
    'toggle_node_info_page' => 0,
    'toggle_node_info_story' => 1,
    'default_logo' => 1,
    'logo_path' => '',
    'logo_upload' => '',
    'default_favicon' => 1,
    'favicon_path' => '',
    'favicon_upload' => '',
    'op' => 'Save configuration',
    'form_token' => '3b7223eff41556589c9bdd1582877070',
  ));
  variable_set('update_access_fixed', true);
  variable_set('update_status_last', 1225136411);
  variable_set('user_email_verification', 1);
  variable_set('user_mail_admin_body', '!username,

  

  A site administrator at !site has created an account for you. You may now log in to !login_uri using the following username and password:

  

  username: !username

  password: !password

  

  You may also log in by clicking on this link or copying and pasting it in your browser:

  

  !login_url

  

  This is a one-time login, so it can be used only once.

  

  After logging in, you will be redirected to !edit_uri so you can change your password.

  

  

  --  !site team');
  variable_set('user_mail_admin_subject', 'An administrator created an account for you at !site');
  variable_set('user_mail_approval_body', '!username,

  

  Thank you for registering at !site. Your application for an account is currently pending approval. Once it has been granted, you may log in to !login_uri using the following username and password:

  

  username: !username

  password: !password

  

  You may also log in by clicking on this link or copying and pasting it in your browser:

  

  !login_url

  

  This is a one-time login, so it can be used only once.

  

  After logging in, you may wish to change your password at !edit_uri

  

  

  --  !site team');
  variable_set('user_mail_approval_subject', 'Account details for !username at !site (pending admin approval)');
  variable_set('user_mail_pass_body', '!username,

  

  A request to reset the password for your account has been made at !site.

  

  You may now log in to !uri_brief by clicking on this link or copying and pasting it in your browser:

  

  !login_url

  

  This is a one-time login, so it can be used only once. It expires after one day and nothing will happen if it\'s not used.

  

  After logging in, you will be redirected to !edit_uri so you can change your password.');
  variable_set('user_mail_pass_subject', 'Replacement login information for !username at !site');
  variable_set('user_mail_welcome_body', '!username,

  

  Thank you for registering at !site. You may now log in to !login_uri using the following username and password:

  

  username: !username

  password: !password

  

  You may also log in by clicking on this link or copying and pasting it in your browser:

  

  !login_url

  

  This is a one-time login, so it can be used only once.

  

  After logging in, you will be redirected to !edit_uri so you can change your password.

  

  

  --  !site team');
  variable_set('user_mail_welcome_subject', 'Account details for !username at !site');
  variable_set('user_pictures', '0');
  variable_set('user_picture_default', '');
  variable_set('user_picture_dimensions', '85x85');
  variable_set('user_picture_file_size', '30');
  variable_set('user_picture_guidelines', '');
  variable_set('user_picture_path', 'pictures');
  variable_set('user_register', '1');
  variable_set('user_registration_help', '');
  variable_set('wipe', 'Re-index site');
}

function ejournal_profile_profile_final_blocks() {
  install_add_block('views', 'comments_recent', 'garland', 0, 0, '', 0, '', 0, 0, '', '', '', 0);
  install_add_block('views', 'comments_on_article', 'garland', 1, -6, 'left', 2, '<?php

if (arg(0) == "node" && is_numeric(arg(1))) {

$node = node_load(arg(1));

if ($node->type == "article") { return TRUE; }

}', 0, 0, '', '', '', 0);
  install_add_block('views', 'articles_recent', 'garland', 1, -8, 'left', 0, '<front>', 0, 0, '', '', '', 0);
  install_add_block('views', 'articles_related_datasheets', 'garland', 0, 0, '', 0, '', 0, 0, '', '', '', 0);
  install_add_block('views', 'articles_in_issue_full', 'garland', 0, 0, '', 0, '', 0, 0, '', '', '', 0);
  install_add_block('views', 'issues_related_picture', 'garland', 1, 0, 'right', 1, 'issues/*', 0, 0, '', '', '', 0);
  install_add_block('views', 'authors_related_articles', 'garland', 1, 0, 'content', 1, 'authors/*', 0, 0, '', '', '', 0);
  install_add_block('views', 'articles_in_issue_teaser', 'garland', 0, 0, '', 0, '', 0, 0, '', '', '', 0);
  install_add_block('views', 'articles_featured_topic', 'garland', 0, 0, '', 0, '', 0, 0, '', '', '', 0);
  install_add_block('views', 'editors_blog', 'garland', 1, 0, 'right', 1, '<front>', 0, 0, '', '', '', 0);
  install_add_block('views', 'front_issues_recent_uncat', 'garland', 0, 0, '', 0, '', 0, 0, '', '', '', 0);
  install_add_block('views', 'front_issues_recent_picture', 'garland', 0, 0, '', 0, '', 0, 0, '', '', '', 0);
  install_add_block('views', 'issues', 'garland', 1, 2, 'left', 0, '', 0, 0, '', '', '', 0);
  install_add_block('views', 'front_issues_recent', 'garland', 0, 0, '', 0, '', 0, 0, '', '', '', 0);
  install_add_block('panels_mini', '1', 'garland', 0, 0, '', 0, '', 0, 0, '', '', '', 0);
  install_add_block('user', '3', 'garland', 0, 0, '', 0, '', 0, 0, '', '', '', 0);
  install_add_block('user', '2', 'garland', 0, 0, '', 0, '', 0, 0, '', '', '', 0);
  install_add_block('user', '1', 'garland', 0, 0, '', 0, '', 0, 0, '', '', '', 0);
  install_add_block('user', '0', 'garland', 0, 0, '', 0, '', 0, 0, '', '', '', 0);
  install_add_block('search', '0', 'garland', 0, 0, '', 0, '', 0, 0, '', '', '', 0);
  install_add_block('node', '0', 'garland', 0, 0, '', 0, '', 0, 0, '', '', '', 0);
  install_add_block('menu', '94', 'garland', 1, 0, 'right', 1, 'users/*', 0, 0, '', '', '', 0);
  install_add_block('menu', '2', 'garland', 0, 0, '', 0, '', 0, 0, '', '', '', 0);
  install_add_block('block', '1', 'garland', 1, 0, 'content', 1, 'user/login

user/register', 0, 0, ' ', '<?php drupal_set_message("Please login or register to the site to comment on the articles or the editor\'s blog."); ?>', 'user_login_welcome_text', 2);
  install_add_block('comment', '0', 'garland', 0, 0, '', 0, '', 0, 0, '', '', '', 0);
  install_add_block('block', '3', 'garland', 1, 10, 'left', 0, '', 0, 0, 'ISSN', '<p><span class=\"issn\"><b>0000-0000</b></span></p><p>The ISSN is configurable or removable in the administration section.</p>', 'ISSN', 3);
  install_add_block('block', '2', 'garland', 1, -2, 'left', 1, '<front>', 0, 0, '', '<?php



$node = node_load(1);

print node_view($node, TRUE);?>', 'about_us_block', 2);
  install_add_block('block', '4', 'garland', 1, 1, 'left', 0, '', 0, 0, 'Topics', '<?php 
     $topic_result = db_query("SELECT tid, name FROM {term_data} WHERE vid = %d", 2);
     $topics = array();
     while ($topic = db_fetch_array($topic_result)) {
       $topics[] = l($topic["name"], "taxonomy/term/" . $topic["tid"]);
     }
     if (count($topics)) {
       return theme("item_list", $topics);
     } else {
       print "No topics have been created.";
     }
  
  ?>', 'Topics', 2);
  install_add_block_role('block', 1, DRUPAL_ANONYMOUS_RID);
  
  // clean up
  cache_clear_all();
  system_initialize_theme_blocks('garland');
}
